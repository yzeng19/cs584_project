import hydra

from src.dataset.gsdc_data_module import *
from src.modeling.gsdc_model import *
from src.postprocess.score import print_metric


@hydra.main(config_path="./src/config", config_name="config")
def main(conf: DictConfig) -> None:
    # prepare data
    data_module = GSDCDataModule(conf)
    train_set, val_set = data_module.train_dataset()

    model = GSDCModel(conf)
    model.train(train_set, val_set, epochs=conf.trainer.max_epochs)
    print('train finish')

    print_metric(val_set)


if __name__ == "__main__":
    main()
