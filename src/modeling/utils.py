import os
import random

import numpy as np
import tensorflow as tf
from keras import backend as K
from tensorflow.keras.activations import relu
from tensorflow.keras.layers import Input, Dense, BatchNormalization, Dropout, Concatenate, Activation
from tensorflow.keras.models import Model
from tensorflow_addons.optimizers import AdamW


def r2(y_true, y_pred):
    a = K.square(y_pred - y_true)
    b = K.sum(a)
    c = K.mean(y_true)
    d = K.square(y_true - c)
    e = K.sum(d)
    f = 1 - b / e
    return f


def my_loss(y_true, y_pred):
    loss_less = 1.0
    loss_more = 2.0

    error = y_pred - y_true
    error_fix = tf.where(tf.greater(y_pred, y_true), error * loss_less, - error * loss_more)
    mae_fix = K.mean(error_fix)
    return mae_fix


def check_random(SEED=2021):
    os.environ['TF_DETERMINISTIC_OPS'] = '1'
    os.environ['PYTHONHASHSEED'] = str(SEED)
    random.seed(SEED)
    np.random.seed(SEED)
    tf.random.set_seed(SEED)


def nns(x, hidden_units, activation, dropout_rates):
    for i in range(len(hidden_units)):
        x = Dense(hidden_units[i])(x)
        x = BatchNormalization()(x)
        x = Activation(activation[i])(x)
        x = Dropout(dropout_rates[i + 1])(x)
    return x


def create_nn_model(num_cols, cat_cols_list, output_dim, learning_rate):
    inp = Input(shape=(num_cols,))

    inp_cat1 = Input(shape=(cat_cols_list[0],))
    inp_cat2 = Input(shape=(cat_cols_list[1],))
    inp_cat3 = Input(shape=(cat_cols_list[2],))
    inp_cat4 = Input(shape=(cat_cols_list[3],))
    inp_cat5 = Input(shape=(cat_cols_list[4],))
    inp_cat6 = Input(shape=(cat_cols_list[5],))
    inp_cat7 = Input(shape=(cat_cols_list[6],))

    dense_cats = [Dense(50)(inp_cat1), Dense(50)(inp_cat2), Dense(50)(inp_cat3), Dense(50)(inp_cat4),
                  Dense(50)(inp_cat5), Dense(50)(inp_cat6), Dense(50)(inp_cat7)]

    x = Concatenate(axis=-1)(dense_cats + [inp])
    x = Dense(512)(x)
    x = Activation(relu)(x)

    x = Dense(512)(x)
    x = Activation(relu)(x)

    x = Dense(256)(x)
    x = Activation(relu)(x)

    x = Dense(128)(x)
    x = Activation(relu)(x)

    x = Dense(64)(x)

    out = Dense(output_dim)(x)

    model = Model(inputs=[inp,
                          inp_cat1, inp_cat2,
                          inp_cat3, inp_cat4,
                          inp_cat5, inp_cat6,
                          inp_cat7], outputs=out)
    model.compile(
        optimizer=AdamW(learning_rate=learning_rate, weight_decay=0.0001),
        loss=[my_loss],
        metrics=['mae', 'mse']
    )

    return model
