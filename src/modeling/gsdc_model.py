import joblib
import lightgbm as lgb
from keras.callbacks import ReduceLROnPlateau, EarlyStopping, ModelCheckpoint
from omegaconf import DictConfig
from sklearn import preprocessing

from src.dataset.kalman_filter import KF
from src.dataset.utils import *
from src.modeling.utils import *


class GSDCModel:
    def __init__(self, conf: DictConfig):
        super().__init__()
        self.ss = preprocessing.StandardScaler()
        self.conf = conf
        self.ann_model = None
        self.speed_mode = None
        self.model_lat = None
        self.model_lng = None

    def pre_processing(self, dataset, is_need_fit=True):
        data_num = dataset[numerical_cols]
        cat_one_hot_1 = pd.get_dummies(dataset[[cat_cols[0]]].astype('str'))
        cat_one_hot_2 = pd.get_dummies(dataset[[cat_cols[1]]].astype('str'))
        cat_one_hot_3 = pd.get_dummies(dataset[[cat_cols[2]]].astype('str'))
        cat_one_hot_4 = pd.get_dummies(dataset[[cat_cols[3]]].astype('str'))
        cat_one_hot_5 = pd.get_dummies(dataset[[cat_cols[4]]].astype('str'))
        cat_one_hot_6 = pd.get_dummies(dataset[[cat_cols[5]]].astype('str'))
        cat_one_hot_7 = pd.get_dummies(dataset[[cat_cols[6]]].astype('str'))

        if is_need_fit:
            data_num = self.ss.fit_transform(data_num)
        else:
            data_num = self.ss.transform(data_num)

        return [data_num, cat_one_hot_1, cat_one_hot_2, cat_one_hot_3,
                cat_one_hot_4, cat_one_hot_5, cat_one_hot_6, cat_one_hot_7]

    def train(self, train_set, val_set, epochs=200, batch_size=1024, learning_rate=0.005):
        check_random()

        x_tr = train_set[feature_cols]
        y_tr = train_set[label_cols]
        x_val = val_set[feature_cols]
        y_val = val_set[label_cols]

        xtr_input = self.pre_processing(x_tr)
        xval_input = self.pre_processing(x_val, False)

        reduce_lr = ReduceLROnPlateau(monitor='val_loss', verbose=1, factor=0.1, patience=8, mode='min')
        early_stop_callback = EarlyStopping(monitor='val_loss', patience=15, restore_best_weights=True)

        model_checkpoint_callback = ModelCheckpoint(
            filepath=self.conf['model_dir'] + '/ann_model.hdf5',
            verbose=10,
            save_weights_only=True,
            monitor='val_loss',
            mode='min',
            save_best_only=True)

        self.ann_model = create_nn_model(xtr_input[0].shape[-1], [21, 21, 21, 21, 21, 21, 7], 1, learning_rate)
        his = self.ann_model.fit(xtr_input, y_tr,
                           epochs=epochs,
                           batch_size=batch_size,
                           validation_data=(xval_input, y_val),
                           callbacks=[reduce_lr, early_stop_callback, model_checkpoint_callback]
                           )
        # plt.plot(his.history['loss'])
        # plt.plot(his.history['val_loss'])
        # plt.title('Model loss')
        # plt.ylabel('Loss')
        # plt.xlabel('Epoch')
        # plt.legend(['Train', 'Test'], loc='upper left')
        # plt.show()
        print('Finish ann_model train')

        model_speed_callback = ModelCheckpoint(
            filepath=self.conf['model_dir'] + '/speed_model.hdf5',
            verbose=10,
            save_weights_only=True,
            monitor='val_loss',
            mode='min',
            save_best_only=True)
        y_speed_tr = train_set[label_speed_cols]
        y_speed_val = val_set[label_speed_cols]

        self.speed_mode = create_nn_model(xtr_input[0].shape[-1], [21, 21, 21, 21, 21, 21, 7], 1, learning_rate)
        his = self.speed_mode.fit(xtr_input,  y_speed_tr,
                            epochs=epochs,
                            batch_size=batch_size,
                            validation_data=(xval_input, y_speed_val),
                            callbacks=[reduce_lr, early_stop_callback, model_speed_callback]
                            )
        # plt.plot(his.history['loss'])
        # plt.plot(his.history['val_loss'])
        # plt.title('Model loss')
        # plt.ylabel('Loss')
        # plt.xlabel('Epoch')
        # plt.legend(['Train', 'Test'], loc='upper left')
        # plt.show()
        print('Finish speed_mode train')

        params = {
            'objective': 'regression_l1',
            'max_bin': 600,
            'learning_rate': 0.01,
            'num_leaves': 80
        }

        lgb_train = lgb.Dataset(x_tr, train_set['dis_lat'])
        lgb_eval = lgb.Dataset(x_val, val_set['dis_lat'], reference=lgb_train)
        result = {}
        self.model_lat = lgb.train(params, lgb_train,
                                   valid_sets=[lgb_train, lgb_eval],
                                   verbose_eval=25,
                                   num_boost_round=10000,
                                   evals_result=result,
                                   early_stopping_rounds=10
                                   )

        joblib.dump(self.model_lat, self.conf['model_dir'] + '/model_lat.pkl')
        print('Finish model_lat train')

        lgb_train = lgb.Dataset(x_tr, train_set['dis_lng'])
        lgb_eval = lgb.Dataset(x_val, val_set['dis_lng'], reference=lgb_train)
        self.model_lng = lgb.train(
            params, lgb_train,
            valid_sets=[lgb_train, lgb_eval],
            verbose_eval=25,
            num_boost_round=10000,
            evals_result=result,
            early_stopping_rounds=10
        )
        joblib.dump(self.model_lng, self.conf['model_dir'] + '/model_lng.pkl')
        print('Finish model_lng train')

    def predict(self, test_set):
        print('start predict')
        x_test = test_set[feature_cols]
        test_input = self.pre_processing(x_test)

        print('load all model param')
        # load param
        self.ann_model = create_nn_model(test_input[0].shape[-1], [21, 21, 21, 21, 21, 21, 7], 1, 0)
        self.ann_model.load_weights(self.conf['model_dir'] + '/ann_model.hdf5')

        self.speed_mode = create_nn_model(test_input[0].shape[-1], [21, 21, 21, 21, 21, 21, 7], 1, 0)
        self.speed_mode.load_weights(self.conf['model_dir'] + '/speed_model.hdf5')

        self.model_lat = joblib.load(self.conf['model_dir'] + '/model_lat.pkl')
        self.model_lng = joblib.load(self.conf['model_dir'] + '/model_lng.pkl')

        print('start ann_model')
        pred_dis = self.ann_model.predict(test_input)
        print('start speed_mode')
        pred_speed = self.speed_mode.predict(test_input)

        x_test['pred_dist_between_baseline_and_truth'] = pred_dis
        x_test['pred_speed_between_baseline_and_truth'] = pred_speed

        test_data_lgb = test_set.copy()
        th_speed_normal = (6.75 + 7.5) / 2
        th_dist_normal = (13.5 + 15) / 2
        regression = test_data_lgb.loc[((x_test['pred_dist_between_baseline_and_truth'] <= th_dist_normal) & (
                x_test['pred_speed_between_baseline_and_truth'] <= th_speed_normal))].copy()

        print('start model_lat')
        dis_lat = self.model_lat.predict(regression[feature_cols])
        print('start model_lng')
        dis_lng = self.model_lng.predict(regression[feature_cols])

        test_data_lgb.loc[((x_test['pred_dist_between_baseline_and_truth'] <= th_dist_normal) & (
                x_test['pred_speed_between_baseline_and_truth'] <= th_speed_normal)), 'latDeg'] -= dis_lat
        test_data_lgb.loc[((x_test['pred_dist_between_baseline_and_truth'] <= th_dist_normal) & (
                x_test['pred_speed_between_baseline_and_truth'] <= th_speed_normal)), 'lngDeg'] -= dis_lng

        print('start kalman_filter')
        kf = KF()
        tmp = test_set.copy()

        th_speed_abnormal = 16
        th_dist_abnormal = 13

        print(len(tmp.loc[((x_test['pred_dist_between_baseline_and_truth'] > th_dist_abnormal) | (
                x_test['pred_speed_between_baseline_and_truth'] > th_speed_abnormal)), :]))
        tmp.loc[((x_test['pred_dist_between_baseline_and_truth'] > th_dist_abnormal) | (
                x_test['pred_speed_between_baseline_and_truth'] > th_speed_abnormal)), ['latDeg', 'lngDeg']] = np.nan
        smoothed_tmp = mean_with_other_phones(tmp)

        kf.apply_kf_smoothing(smoothed_tmp, "")

        print('save sample_submission')
        ata_sub = pd.read_csv(self.conf['data_dir'] + '/' + 'sample_submission.csv')
        df_sub_lgb_best = ata_sub[['phone', 'millisSinceGpsEpoch']].merge(
            smoothed_tmp[['phone', 'millisSinceGpsEpoch', 'latDeg', 'lngDeg']], on=['phone', 'millisSinceGpsEpoch'],
            how='inner')

        df_sub_lgb_best.to_csv(self.conf['out_dir'] + "/submission.csv", index=False)
        print('finish sample_submission')
