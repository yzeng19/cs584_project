import numpy as np
import pandas as pd

from src.dataset.utils import calc_haversine


def calc_metric(pred_df: pd.DataFrame, target_col: str = "dist_err") -> pd.DataFrame:
    val_df2 = pd.DataFrame()
    val_df2["phone"] = pred_df.phone.unique().tolist()
    val_df2["dist_50"] = [
        np.percentile(pred_df[pred_df.phone == ph][target_col], 50)
        for ph in val_df2["phone"].tolist()
    ]
    val_df2["dist_95"] = [
        np.percentile(pred_df[pred_df.phone == ph][target_col], 95)
        for ph in val_df2["phone"].tolist()
    ]
    val_df2["avg_dist_50_95"] = (val_df2["dist_50"] + val_df2["dist_95"]) / 2.0
    print("Val evaluation details:\n", val_df2)
    print("Val evaluation details:\n", val_df2["avg_dist_50_95"].mean())
    return val_df2


def print_metric(df: pd.DataFrame):
    df["dist_err_orig"] = calc_haversine(
        df["latDeg_truth"], df["lngDeg_truth"], df["latDeg"], df["lngDeg"],
    )
    met_df = calc_metric(pred_df=df, target_col="dist_err_orig")
    return met_df
