from omegaconf import DictConfig
from sklearn.model_selection import train_test_split

from src.dataset.kalman_filter import *
from src.dataset.utils import *


class GSDCDataModule:
    def __init__(self, conf: DictConfig):
        super().__init__()
        self.conf = conf
        Path(self.conf['model_dir']).mkdir(exist_ok=True)
        Path(self.conf['out_dir']).mkdir(exist_ok=True)
        # pre process origin data
        if self.conf.trainer.enable_parse_gnss:
            parse_gnss_log(Path(self.conf['data_dir']))

    def prepare_data(self, train_data, test_data):
        OUT_DIR = Path("./")
        if not self.conf.trainer.enable_parse_gnss:
            OUT_DIR = Path(self.conf['out_dir'])

        idx_of_phonename = {phoneName: idx for idx, phoneName in
                            enumerate(test_data.phoneName.drop_duplicates().to_list())}
        train_data["phoneCat"] = train_data["phoneName"].map(idx_of_phonename)
        test_data["phoneCat"] = test_data["phoneName"].map(idx_of_phonename)

        # pre process test data
        data_processing(train_data)
        data_processing(test_data)

        train_derived_data = get_union(Path(self.conf['data_dir']) / 'train', "*_derived.csv")
        train_data = derived_data_processing(train_derived_data, train_data)
        test_derived_data = get_union(Path(self.conf['data_dir']) / 'test', "*_derived.csv")
        test_data = derived_data_processing(test_derived_data, test_data)

        bucket_data_processing(train_data, test_data)

        # raw
        train_raw_data = get_union(OUT_DIR / 'train', "Raw.csv")
        train_data = raw_data_processing(train_raw_data, train_data)
        test_raw_data = get_union(OUT_DIR / 'test', "Raw.csv")
        test_data = raw_data_processing(test_raw_data, test_data)

        # sensor
        train_gyro_data = get_union(OUT_DIR / 'train', "UncalGyro.csv")
        train_mag_data = get_union(OUT_DIR / 'train', "UncalMag.csv")
        train_accel_data = get_union(OUT_DIR / 'train', "UncalAccel.csv")
        train_data = sensor_data_processing(train_gyro_data, train_mag_data, train_accel_data, train_data)

        test_gyro_data = get_union(OUT_DIR / 'test', "UncalGyro.csv")
        test_mag_data = get_union(OUT_DIR / 'test', "UncalMag.csv")
        test_accel_data = get_union(OUT_DIR / 'test', "UncalAccel.csv")
        test_data = sensor_data_processing(test_gyro_data, test_mag_data, test_accel_data, test_data)

        # kalman filter
        kf = KF()
        kf.apply_kf_smoothing(train_data, suffix="_kf")
        kf.apply_kf_smoothing(test_data, suffix="_kf")
        kf.add_kf_features(train_data)
        kf.add_kf_features(test_data)

        loss_data_processing(train_data)
        loss_data_processing(test_data)
        return train_data, test_data

    def train_dataset(self):
        if not self.conf.trainer.enable_rebuid_pd:
            train_data = pd.read_csv(Path(self.conf['out_dir']) / "train_pd.csv")
        else:
            gt_data = get_union(Path(self.conf['data_dir']) / "train", "ground_truth.csv")
            train_data = pd.read_csv(Path(self.conf['data_dir']) / "baseline_locations_train.csv")
            test_data = pd.read_csv(Path(self.conf['data_dir']) / "baseline_locations_test.csv")

            gt_data.rename(columns={
                "latDeg": "latDeg_truth",
                "lngDeg": "lngDeg_truth",
                "heightAboveWgs84EllipsoidM": "heightAboveWgs84EllipsoidM_truth"}, inplace=True)
            train_data = train_data.merge(gt_data, on=['collectionName', 'phoneName', 'millisSinceGpsEpoch'])

            train_data["dist_between_baseline_and_truth"] = calc_haversine(
                train_data.latDeg, train_data.lngDeg,
                train_data.latDeg_truth, train_data.lngDeg_truth
            )

            train_data, test_data = self.prepare_data(train_data, test_data)
            # speed
            train_data = create_shift_features(train_data, shift_list, shift_columns, group_name, use_cols)
            train_data = create_timediff_v_a(train_data)
            train_data['speed_between_baseline_and_truth'] = abs(train_data['speed'] - train_data['speedMps']).fillna(0)

            train_data['dis_lat'] = train_data['latDeg'] - train_data['latDeg_truth']
            train_data['dis_lng'] = train_data['lngDeg'] - train_data['lngDeg_truth']
            th_speed_normal = (6.75 + 7.5) / 2
            th_dist_normal = (13.5 + 15) / 2
            train_data = train_data.loc[((train_data["dist_between_baseline_and_truth"] <= th_dist_normal) & (
                    train_data["speed_between_baseline_and_truth"] <= th_speed_normal))].copy()

            train_data.to_csv(Path(self.conf['out_dir']) / "train_pd.csv")
        train_pd, val_pd = train_test_split(train_data, test_size=0.2)
        return train_pd, val_pd

    def test_dataset(self):
        if not self.conf.trainer.enable_rebuid_pd:
            test_data = pd.read_csv(Path(self.conf['out_dir']) / "test_pd.csv")
        else:
            train_data = pd.read_csv(Path(self.conf['data_dir']) / "baseline_locations_train.csv")
            test_data = pd.read_csv(Path(self.conf['data_dir']) / "baseline_locations_test.csv")

            test_data = create_shift_features(test_data, shift_list, shift_columns, group_name, use_cols)
            test_data = create_timediff_v_a(test_data)

            train_data, test_data = self.prepare_data(train_data, test_data)
            test_data.to_csv(Path(self.conf['out_dir']) / "test_pd.csv")
        return test_data
