import numpy as np
import pandas as pd
import simdkalman
from tqdm import tqdm

from src.dataset.utils import calc_haversine


class KF:
    def __init__(self):
        T = 1.0
        state_transition = np.array([[1, 0, T, 0, 0.5 * T ** 2, 0],
                                     [0, 1, 0, T, 0, 0.5 * T ** 2],
                                     [0, 0, 1, 0, T, 0],
                                     [0, 0, 0, 1, 0, T],
                                     [0, 0, 0, 0, 1, 0],
                                     [0, 0, 0, 0, 0, 1]])
        process_noise = np.diag([1e-5, 1e-5, 5e-6, 5e-6, 1e-6, 1e-6]) + np.ones((6, 6)) * 1e-9
        observation_model = np.array([[1, 0, 0, 0, 0, 0], [0, 1, 0, 0, 0, 0]])
        observation_noise = np.diag([5e-5, 5e-5]) + np.ones((2, 2)) * 1e-9
        self.kf = simdkalman.KalmanFilter(
            state_transition=state_transition,
            process_noise=process_noise,
            observation_model=observation_model,
            observation_noise=observation_noise)

    def apply_kf_smoothing(self, df, suffix):
        phones = df["phone"].drop_duplicates().tolist()
        for phone in tqdm(phones):
            cond = df['phone'] == phone
            tmp = df[cond].copy()
            tmp[0] = tmp["millisSinceGpsEpoch"] // 1000
            tmp = tmp.merge(pd.DataFrame(range(tmp[0].min(), tmp[0].max() + 1)), on=[0], how="right")
            tmp_np = tmp[['latDeg', 'lngDeg']].to_numpy()
            nan_idxs = tmp[tmp["millisSinceGpsEpoch"].isnull()].index.to_list()
            tmp_np = tmp_np.reshape(1, len(tmp_np), 2)
            smoothed = self.kf.smooth(tmp_np).states.mean
            smoothed = np.delete(smoothed, list(nan_idxs), 1)
            df.loc[cond, 'latDeg' + suffix] = smoothed[0, :, 0]
            df.loc[cond, 'lngDeg' + suffix] = smoothed[0, :, 1]

    def add_kf_features(self, data):
        data["dist_between_base_and_kf"] = calc_haversine(data.latDeg, data.lngDeg, data.latDeg_kf, data.lngDeg_kf)
        data["base_kf_lat_delta"] = data["latDeg"] - data["latDeg_kf"]
        data["base_kf_lng_delta"] = data["lngDeg"] - data["lngDeg_kf"]
        data["abs_base_kf_lng_delta"] = abs(data["lngDeg"] - data["lngDeg_kf"])
        data["abs_base_kf_lat_delta"] = abs(data["latDeg"] - data["latDeg_kf"])
