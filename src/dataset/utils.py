import os
import random
from pathlib import Path

import numpy as np
import pandas as pd
import tensorflow as tf
from scipy.interpolate import interp1d
from tqdm import tqdm

from src.dataset.feature import *


def get_union(dir_name, file_name):
    print("Reading", file_name, "from", dir_name, "...")
    datas = []
    for path in tqdm(Path(dir_name).rglob(file_name)):
        datas.append(pd.read_csv(path, low_memory=False, index_col=False))
    return pd.concat(datas)


def check_random(seed=20):
    os.environ['TF_DETERMINISTIC_OPS'] = '1'
    os.environ['PYTHONHASHSEED'] = str(seed)
    random.seed(seed)
    np.random.seed(seed)
    tf.random.set_seed(seed)


def calc_haversine(lat1, lng1, lat2, lng2):
    """from
    https://www.kaggle.com/dehokanta/baseline-post-processing-by-outlier-correction
    """
    RADIUS = 6_367_000
    lat1, lng1, lat2, lng2 = map(np.radians, [lat1, lng1, lat2, lng2])
    dlat = lat2 - lat1
    dlng = lng2 - lng1
    a = np.sin(dlat / 2) ** 2 + \
        np.cos(lat1) * np.cos(lat2) * np.sin(dlng / 2) ** 2
    dist = 2 * RADIUS * np.arcsin(a ** 0.5)
    return dist


def create_timediff_v_a(df):
    # df = pd_train
    df_ = df.copy()
    df_['millisSinceGpsEpoch_diff'] = df_['millisSinceGpsEpoch_shift_-1'] - df_['millisSinceGpsEpoch']

    df_['dis'] = df_.apply(
        lambda x: calc_haversine(x['latDeg'], x['lngDeg'], x['latDeg_shift_-1'], x['lngDeg_shift_-1']), axis=1)
    df_['speed'] = 1000 * df_['dis'] / df_['millisSinceGpsEpoch_diff']
    return df_


def create_shift_features(df, shift_list, shift_columns, group_name, use_col):
    tmp_shift = df.copy()

    for shift_i in shift_list:
        tmp_test = tmp_shift.groupby(group_name).shift(shift_i)
        for col in shift_columns:
            tmp_name = col + '_shift_' + str(shift_i)
            if tmp_name not in use_col:
                tmp_shift[tmp_name] = tmp_test[col]
            else:
                print('out col : {}'.format(tmp_name))
                continue
    return tmp_shift


def mean_with_other_phones(df):
    """
    https://www.kaggle.com/bpetrb/adaptive-gauss-phone-mean
    """
    collections_list = df[['collectionName']].drop_duplicates().to_numpy()

    for collection in collections_list:
        phone_list = df[df['collectionName'].to_list() == collection][['phoneName']].drop_duplicates().to_numpy()

        phone_data = {}
        corrections = {}
        for phone in phone_list:
            cond = np.logical_and(df['collectionName'] == collection[0], df['phoneName'] == phone[0]).to_list()
            phone_data[phone[0]] = df[cond][['millisSinceGpsEpoch', 'latDeg', 'lngDeg']].to_numpy()

        for current in phone_data:
            correction = np.ones(phone_data[current].shape, dtype=np.float)
            correction[:, 1:] = phone_data[current][:, 1:]

            # Telephones data don't complitely match by time, so - interpolate.
            for other in phone_data:
                if other == current:
                    continue

                loc = interp1d(phone_data[other][:, 0],
                               phone_data[other][:, 1:],
                               axis=0,
                               kind='linear',
                               copy=False,
                               bounds_error=None,
                               fill_value='extrapolate',
                               assume_sorted=True)

                start_idx = 0
                stop_idx = 0
                for idx, val in enumerate(phone_data[current][:, 0]):
                    if val < phone_data[other][0, 0]:
                        start_idx = idx
                    if val < phone_data[other][-1, 0]:
                        stop_idx = idx

                if stop_idx - start_idx > 0:
                    correction[start_idx:stop_idx, 0] += 1
                    correction[start_idx:stop_idx, 1:] += loc(phone_data[current][start_idx:stop_idx, 0])

            correction[:, 1] /= correction[:, 0]
            correction[:, 2] /= correction[:, 0]

            corrections[current] = correction.copy()

        for phone in phone_list:
            cond = np.logical_and(df['collectionName'] == collection[0], df['phoneName'] == phone[0]).to_list()

            df.loc[cond, ['latDeg', 'lngDeg']] = corrections[phone[0]][:, 1:]
    return df


def parse_gnss_log(data_dir):
    header_of_table_named = {
        "UncalMag":
            "utcTimeMillis,elapsedRealtimeNanos,UncalMagXMicroT,UncalMagYMicroT,UncalMagZMicroT\n",
        "UncalAccel":
            "utcTimeMillis,elapsedRealtimeNanos,UncalAccelXMps2,UncalAccelYMps2,UncalAccelZMps2\n",
        "UncalGyro":
            "utcTimeMillis,elapsedRealtimeNanos,UncalGyroXRadPerSec,UncalGyroYRadPerSec,UncalGyroZRadPerSec\n",
        "Status":
            "UnixTimeMillis,SignalCount,SignalIndex,ConstellationType,Svid,CarrierFrequencyHz,Cn0DbHz,"
            "AzimuthDegrees,ElevationDegrees,UsedInFix,HasAlmanacData,HasEphemerisData\n",
        "Raw":
            "utcTimeMillis,TimeNanos,LeapSecond,TimeUncertaintyNanos,FullBiasNanos,BiasNanos,BiasUncertaintyNanos,"
            "DriftNanosPerSecond,DriftUncertaintyNanosPerSecond,HardwareClockDiscontinuityCount,"
            "Svid,TimeOffsetNanos,State,ReceivedSvTimeNanos,ReceivedSvTimeUncertaintyNanos,Cn0DbHz,"
            "PseudorangeRateMetersPerSecond,PseudorangeRateUncertaintyMetersPerSecond,AccumulatedDeltaRangeState,"
            "AccumulatedDeltaRangeMeters,AccumulatedDeltaRangeUncertaintyMeters,CarrierFrequencyHz,CarrierCycles,"
            "CarrierPhase,CarrierPhaseUncertainty,MultipathIndicator,SnrInDb,ConstellationType,AgcDb\n",
        "Fix":
            "Provider,LatitudeDegrees,LongitudeDegrees,AltitudeMeters,SpeedMps,AccuracyMeters,BearingDegrees,"
            "UnixTimeMillis,SpeedAccuracyMps,BearingAccuracyDegrees\n"
    }
    for part in ["train", "test"]:
        print(part, "gnsslog parsing ...")
        for file_name in Path(data_dir / part).rglob("*GnssLog.txt"):
            print(file_name)
            with open(str(file_name)) as f_open:
                datalines = f_open.readlines()
            f_named = {}
            for f_name in header_of_table_named.keys():
                dir_pth = Path('/'.join(str(file_name.parent).split("/")[-3:]))
                Path(dir_pth.parent.parent).mkdir(exist_ok=True)
                Path(dir_pth.parent).mkdir(exist_ok=True)
                Path(dir_pth).mkdir(exist_ok=True)
                output_filename = dir_pth / (f_name + ".csv")
                if not output_filename.exists():
                    f_named[f_name] = open(output_filename, "w")
            if f_named:
                collectionName = file_name.parent.parent.name
                phoneName = file_name.parent.name
                for f_name, f in f_named.items():
                    f.write("collectionName,phoneName," + header_of_table_named[f_name])
                for dataline in tqdm(datalines):
                    for f_name in f_named.keys():
                        if dataline.startswith(f_name):
                            f_named[f_name].write(collectionName)
                            f_named[f_name].write(',')
                            f_named[f_name].write(phoneName)
                            f_named[f_name].write(',')
                            f_named[f_name].write(dataline[len(f_name) + 1:])
                            break
                for f in f_named.values():
                    f.close()
    print("GnssLog finish")


def data_processing(data):
    data[["month", "day"]] = data['collectionName'].str.split('-', expand=True)[[1, 2]]
    data["ratio_of_year"] = (30 * (data.month.astype(int) - 1) + data.day.astype(int)) / 365

    offsets = [1, 2, 3, 4, 5]
    groups = data[["phone", "latDeg", "lngDeg"]].groupby("phone")
    for offset in offsets:
        data[["mean_latDeg+-" + str(offset), "mean_lngDeg+-" + str(offset)]] = \
            groups.rolling(window=3, min_periods=2, center=True).mean().values
        data["latDeg_mean_delta_" + str(offset)] = np.abs(data["mean_latDeg+-" + str(offset)] - data["latDeg"])
        data["lngDeg_mean_delta_" + str(offset)] = np.abs(data["mean_lngDeg+-" + str(offset)] - data["lngDeg"])

    data[["latDeg-1", "lngDeg-1"]] = groups.shift(1)
    data[["latDeg+1", "lngDeg+1"]] = groups.shift(-1)
    data["latDeg_pre_increment"] = data["latDeg"] - data["latDeg-1"]
    data["lngDeg_pre_increment"] = data["lngDeg"] - data["lngDeg-1"]
    data["latDeg_post_increment"] = data["latDeg+1"] - data["latDeg"]
    data["lngDeg_post_increment"] = data["lngDeg+1"] - data["lngDeg"]
    data["dist_pre"] = calc_haversine(data["latDeg"], data["lngDeg"], data["latDeg-1"], data["lngDeg-1"])
    data["dist_post"] = calc_haversine(data["latDeg"], data["lngDeg"], data["latDeg+1"], data["lngDeg+1"])

    nan_idxs = data[data.latDeg_pre_increment.isnull()].index
    data.loc[nan_idxs, ["latDeg_pre_increment", "lngDeg_pre_increment", "dist_pre"]] = \
        data.loc[nan_idxs + 1, ["latDeg_pre_increment", "lngDeg_pre_increment", "dist_pre"]].values
    nan_idxs = data[data.latDeg_post_increment.isnull()].index
    data.loc[nan_idxs, ["latDeg_post_increment", "lngDeg_post_increment", "dist_post"]] = \
        data.loc[nan_idxs - 1, ["latDeg_post_increment", "lngDeg_post_increment", "dist_post"]].values

    data["latDeg_pre_post_mean_abs_delta"] = (np.abs(data["latDeg_pre_increment"]) +
                                              np.abs(data["latDeg_post_increment"])) / 2
    data["lngDeg_pre_post_mean_abs_delta"] = (np.abs(data["lngDeg_pre_increment"]) +
                                              np.abs(data["lngDeg_post_increment"])) / 2
    data["pre_post_mean_abs_dist"] = (np.abs(data["dist_pre"]) + np.abs(data["dist_post"])) / 2

    col_names = ["latDeg", "lngDeg", "heightAboveWgs84EllipsoidM"]
    fn_names = ["max", "min", "mean", "sum", "count"]
    groups = data.groupby("millisSinceGpsEpoch")
    for fn_name_ in fn_names:
        data[[fn_name_ + '_' + col_name + "_with_same_millisSinceGpsEpoch" for col_name in col_names]] = \
            groups[col_names].transform(fn_name_).values

    data["dist_between_baseline_and_phone_mean"] = calc_haversine(data.latDeg, data.lngDeg,
                                                                  data.mean_latDeg_with_same_millisSinceGpsEpoch,
                                                                  data.mean_lngDeg_with_same_millisSinceGpsEpoch)


def derived_data_processing(derived_data, data):
    derived_data["correctedPrm"] = derived_data["rawPrM"] + derived_data["satClkBiasM"] - \
                                   derived_data["isrbM"] - derived_data["ionoDelayM"] - \
                                   derived_data["tropoDelayM"]

    derived_data["signalType_svid"] = derived_data["signalType"] + '_' + derived_data["svid"].astype("string")

    data["millisSinceGpsEpoch/1000_round"] = np.round(data["millisSinceGpsEpoch"] / 1000).astype(np.int64)
    derived_data["millisSinceGpsEpoch/1000_round"] = np.round(derived_data["millisSinceGpsEpoch"] / 1000).astype(
        np.int64)

    groups = derived_data.groupby(["collectionName", "phoneName", "millisSinceGpsEpoch/1000_round"])
    data = data.merge(
        groups["signalType_svid"].agg(lambda group: group.values),
        on=["collectionName", "phoneName", "millisSinceGpsEpoch/1000_round"], how="left"
    )
    data = data.merge(
        groups[["correctedPrm", "rawPrUncM", "satClkDriftMps"]].mean().rename(columns={
            "correctedPrm": "correctedPrm_avg",
            "rawPrUncM": "rawPrUncM_avg",
            "satClkDriftMps": "satClkDriftMps_avg"
        }),
        on=["collectionName", "phoneName", "millisSinceGpsEpoch/1000_round"], how="left"
    )
    data = data.merge(
        groups[["correctedPrm", "rawPrUncM", "satClkDriftMps"]].std().rename(columns={
            "correctedPrm": "correctedPrm_std",
            "rawPrUncM": "rawPrUncM_std",
            "satClkDriftMps": "satClkDriftMps_std"
        }),
        on=["collectionName", "phoneName", "millisSinceGpsEpoch/1000_round"], how="left"
    )

    return data


def bucket_data_processing(train_data, test_data):
    num_buckets = 20
    attr_names = ["correctedPrm", "rawPrUncM", "satClkDriftMps"]
    concat_data = pd.concat([train_data, test_data])
    for fn_name in ["avg", "std"]:
        for attr_name in attr_names:
            concat_data[attr_name + '_' + fn_name + "_bucketized"] = pd.qcut(concat_data[attr_name + '_' + fn_name],
                                                                             num_buckets, labels=False)
            concat_data.loc[concat_data[attr_name + '_' + fn_name + "_bucketized"].isnull(),
                            attr_name + '_' + fn_name + "_bucketized"] = num_buckets
            concat_data[attr_name + '_' + fn_name + "_bucketized"] = \
                concat_data[attr_name + '_' + fn_name + "_bucketized"].astype(np.int64)
            train_data[attr_name + '_' + fn_name + "_bucketized"] = \
                concat_data[:len(train_data)][attr_name + '_' + fn_name + "_bucketized"]
            test_data[attr_name + '_' + fn_name + "_bucketized"] = \
                concat_data[-len(test_data):][attr_name + '_' + fn_name + "_bucketized"]


def raw_data_processing(raw_data, data):
    raw_data["millisSinceGpsEpoch"] = np.round((raw_data.TimeNanos - raw_data.FullBiasNanos) / 1000000). \
        astype(np.int64)
    raw_data["millisSinceGpsEpoch/1000_round"] = np.round(raw_data["millisSinceGpsEpoch"] / 1000).astype(np.int64)
    groups = raw_data.groupby(["collectionName", "phoneName", "millisSinceGpsEpoch/1000_round"])
    data = data.merge(
        groups[["Cn0DbHz", "BiasUncertaintyNanos"]].mean().rename(columns={
            "Cn0DbHz": "Cn0DbHz_avg",
            "BiasUncertaintyNanos": "BiasUncertaintyNanos_avg",
        }),
        on=["collectionName", "phoneName", "millisSinceGpsEpoch/1000_round"], how="left"
    )
    data = data.merge(
        groups[["Cn0DbHz", "BiasUncertaintyNanos"]].std().rename(columns={
            "Cn0DbHz": "Cn0DbHz_std",
            "BiasUncertaintyNanos": "BiasUncertaintyNanos_std",
        }),
        on=["collectionName", "phoneName", "millisSinceGpsEpoch/1000_round"], how="left"
    )
    data = data.merge(
        groups["utcTimeMillis"].first(),
        on=["collectionName", "phoneName", "millisSinceGpsEpoch/1000_round"], how="left"
    )
    return data


def sensor_data_processing(gyro_data, mag_data, accel_data, data):
    data["utcTimeMillis/1000_round"] = np.round(data.utcTimeMillis / 1000).astype(np.int64)
    axises = ['X', 'Y', 'Z']
    prefix_suffix_df_dicts = [
        {"prefix": "UncalAccel", "suffix": "Mps2", "sensor_data": accel_data},
        {"prefix": "UncalGyro", "suffix": "RadPerSec", "sensor_data": gyro_data},
        {"prefix": "UncalMag", "suffix": "MicroT", "sensor_data": mag_data}
    ]
    for prefix_suffix_df_dict in prefix_suffix_df_dicts:
        prefix = prefix_suffix_df_dict["prefix"]
        suffix = prefix_suffix_df_dict["suffix"]
        sensor_data = prefix_suffix_df_dict["sensor_data"]
        sensor_data["utcTimeMillis"] = sensor_data.utcTimeMillis.astype(np.int64)
        sensor_data["utcTimeMillis/1000_round"] = np.round(sensor_data.utcTimeMillis / 1000).astype(np.int64)
        groups = sensor_data.groupby(["collectionName", "phoneName", "utcTimeMillis/1000_round"])
        data = data.merge(groups[[prefix + axis + suffix for axis in axises]].mean().rename(columns={
            prefix + axis + suffix: prefix + axis + suffix + "_avg" for axis in axises
        }), on=["collectionName", "phoneName", "utcTimeMillis/1000_round"], how="left")
        data = data.merge(groups[[prefix + axis + suffix for axis in axises]].std().rename(columns={
            prefix + axis + suffix: prefix + axis + suffix + "_std" for axis in axises
        }), on=["collectionName", "phoneName", "utcTimeMillis/1000_round"], how="left")

    groups = data.groupby(["collectionName", "phoneName"])
    data[[col_name + "+1" for col_name in col_names]] = groups[col_names].shift(-1)

    nan_idxs = data[data["UncalAccelXMps2_avg+1"].isnull()].index
    data.loc[nan_idxs, [col_name + "+1" for col_name in col_names]] = data.loc[nan_idxs, col_names].values

    data["ratio"] = (data["utcTimeMillis"] % 1000) / 1000
    data[col_names] = data[col_names].values * (1 - data["ratio"]).values.reshape(-1, 1) + \
                      data[[col_name + "+1" for col_name in col_names]].values * data["ratio"].values.reshape(-1, 1)

    return data


def loss_data_processing(data):
    data[col_names] = data[col_names].fillna(data[col_names].mean())
