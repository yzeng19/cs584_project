import hydra

from src.dataset.gsdc_data_module import *
from src.modeling.gsdc_model import *


@hydra.main(config_path="./src/config", config_name="config")
def main(conf: DictConfig) -> None:
    # prepare data
    data_module = GSDCDataModule(conf)
    test_set = data_module.test_dataset()

    model = GSDCModel(conf)
    model.predict(test_set)

    print('test finish')


if __name__ == "__main__":
    main()
